%!TEX root = ../paper.tex
\section{Introduction}\label{sec:introduction}
Order dependencies (OD) are a novel field of study that can play an important role in database design. They are closely related to Functional Dependencies (FD) \cite{bitton1989}, but more expressive than the latter. Indeed, ODs state semantic relationships between two order specifications, whereas FDs are relationships between attribute values.

The first main contribution to the formalization of the order dependencies is given by \cite{szlichta2012,szlichta2013}, which provide a sound and complete axiomatization that allows to design efficient inference procedures. Moreover, they state the OD inference problem, proving both the $\mathbb{NP}$-completeness and the relationship with the FD inference task. 

With the increasing complexity of business-intelligence applications and the growth of data stored in the databases, there is a stronger need for query optimizers able to exploit semantic information in designing the query plan.
In particular, the use of indexes or sorting allows to produce efficient access plans. Since sorting is a very expensive task, we should avoid it as much as possible. For this reason, a query optimizer can certainly benefit from knowing the ODs \cite{paulley2001,simmen1996}.

\vspace{-9pt}
\mpara{Motivating Example}
Consider the following SQL query used to retrieve data from relation \texttt{Tickets} shown in Table~\ref{tab:example}.
\vspace{-18pt}
\begin{verbatim}
select T.order, T.departure, T.flight
from Tickets T
where T.price < 200
order by T.departure, T.flight
\end{verbatim}
\vspace{-9pt}
Each row in the table describes the purchase of a flight ticket. Each transaction has a unique \texttt{order} value. By looking at the values on the other columns, we can easily see that \texttt{price} depends on \texttt{class}, while \texttt{flight} is related to \texttt{departure}. In particular, in both cases ordering the tuples by the former will order them by the latter.
The query optimizer can take advantage of this information when deciding how to accomplish the order by clause. 
Indeed, even though the query specifies the answer to be ordered by both \texttt{departure} and \texttt{flight}, taking out one of the attributes from the clause does not change the result. Therefore, the query can be rewritten omitting one of the useless expensive sorting operation. 
\vspace{-9pt}
\begin{table}[!Ht]
\centering
\begin{tabular}{l l l l r}
\toprule
\multicolumn{5}{c}{\textbf{Tickets}} \\
\midrule
\multicolumn{1}{c}{\textbf{order}} & \multicolumn{1}{c}{\textbf{flight}} & \multicolumn{1}{c}{\textbf{departure}} & \multicolumn{1}{c}{\textbf{class}} & \multicolumn{1}{c}{\textbf{price}} \\
\midrule
FA075 & AFL2348 & HAM & Economy & 137 \\
FA114 & NAX11S & KKN & Economy & 100 \\
FP164 & KLM868 & KIX & Economy & 80 \\
JP173 & ACA883 & CPH & Business & 200 \\
JA100 & TAP762M & LIS & Business & 300 \\
YA950 & AFL2347 & HAM & First & 450 \\
\bottomrule
\end{tabular}
\caption{Transactional table recording information about flight tickets bought.}
\label{tab:example}
\end{table}

\mpara{Outline}
In Section~\ref{sec:problem} we state the problem of discovering order dependencies. Section~\ref{sec:solution} presents and formally validates the solution proposed. Section~\ref{sec:experiment} collects and discusses the experimental results and the scalability of the algorithm. Finally, conclusions and directions of future works are provided in Section~\ref{sec:conclusion}. 