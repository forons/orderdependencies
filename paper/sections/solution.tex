%!TEX root = ../paper.tex
\section{The OD Searcher}
\label{sec:solution}
In this section we describe the ideas behind our OD searcher, called \textsc{ODin0}, together with the theory that validates its pruning strategy. Finally, we provide some considerations about the computational cost of the task of OD discovery.

\subsection{Implementation}
\textsc{ODin0} adopts a bottom-up approach, starting from singleton sets of attributes, extending them by adding a new attribute at a time, hence building a lattice level by level. A visual representation of such lattice is shown in Fig.~\ref{fig:lattice}. 

\begin{figure}[!Ht]
\centering
\begin{tikzpicture}[->,>=stealth',shorten >=0.5pt,auto,node distance=2.5cm,scale=0.6,transform shape]
  \node[entity] (1) {AB};
  \node[entity, right of=1] (2) {AC};
  \node[entity, right of=2] (3) {BC};
  \node[entity, below of=1] (4) {A};
  \node[entity, right of=4] (5) {B};
  \node[entity, right of=5] (6) {C};
  \node[entity, above of=2] (7) {ABC};
  \node[entity, below of=5] (8) {$\emptyset$};

    \path[every node/.style={font=\sffamily\small\bfseries}]
      (1) edge node {} (7)
      (2) edge node {} (7)
      (3) edge node {} (7)
      (4) edge node {} (1)
      (4) edge node {} (2)
      (5) edge node {} (1)
      (5) edge node {} (3)
      (6) edge node {} (2)
      (6) edge node {} (3)
      (8) edge node {} (4)
      (8) edge node {} (5)
      (8) edge node {} (6);
  \end{tikzpicture}
  \caption{Lattice for attribute list $\mathcal{A}=\{A, B, C\}$.}
  \label{fig:lattice}
\end{figure}

The lattice is implemented as a queue $lattice$. At the beginning, the algorithm inserts a node for each attribute in the set $\mathcal{C}$, while in the last step the queue will contain a single node composed by all the attributes.
Each node $\mathfrak{X}$ is characterized by a set of attributes $node.cols = \{A_1, \dots, A_l\}$, and a set of candidate order dependencies $node.combs$. The latter are computed by \textsc{genDep} during the initialization of the node. Such procedure exploits the sets of dependencies provided by the lower levels $node.oldDep$ to generate the rules corresponding to that node more efficiently. In particular, it iterates over each element $oldDep$ in $oldList$, creating all its possible extensions by adding an additional attribute. The task is accomplished inserting the \emph{missing attribute} in each position of the right-hand-side (rhs) of the $oldDep$. The missing attribute is some $A_i$ that does not belongs to the node that first created $oldDep$.
The remaining rules are created in the for loop in Lines~22--26, where the $k$-permutations of $node.cols$ are used as rhs for each permutation of $node.cols$. 

Procedure~\textsc{genDep} tests the dependencies in $node.combs$, discarding both the trivial ones and those for which the validity has already been proven.
Every valid rule is inserted in the result set $\mathfrak{D}$, thus the latter will not contain only the minimal order dependencies.  
When a dependency holds, \textsc{applyInference} is called to state the validity of several other rules, based on the pruning strategy explained in Subsection~\ref{subsec:validation}. Such rules are marked as \emph{true}, hence they will not be further checked. Notice that we distinguish four types of values, namely \emph{String}, \emph{Date}, \emph{Long}, and \emph{Double}, hence we compare the tuple values according to the corresponding comparator.

Once a node has been processed, it is extended and set $node.combs$ is passed to all these new nodes.

\begin{algorithm}[!Ht]
	\begin{algorithmic}[1]
		\Require{Set of columns $C$}
		\Ensure{Set of order dependencies $\mathfrak{O}$}
		\State $lattice \gets \emptyset$
		\ForAll{$c \in C$}
			\State $node \gets Node(c, \emptyset)$
			\State $lattice.add(node)$
		\EndFor
		\While{$lattice \neq \emptyset$}
			\State $node \gets lattice.poll()$
			\State $\Call{initialize}{node}$
			\ForAll{$comb \in node.combs$}
				\State $\Call{checkOrderDep}{comb}$
			\EndFor
			\State $lattice.add(\Call{augmentNode}{node, C})$
		\EndWhile
		\Statex
		\Procedure{initialize}{$node$}
			\State $permList \gets \Call{genPerm}{node.cols}$
			\State $\Call{genDep}{node.oldDep, permList}$
		\EndProcedure
		\Statex
		\Procedure{genDep}{$oldList, permList$}
			\ForAll{$oldDep \in oldList$}
				\State Add $missingCol$ in the rhs of $oldDep$
			\EndFor
			\ForAll{$p \in permList$}
				\ForAll{$k$-perm of cols in $node.cols$}
					\State Create dependency: $p \Rightarrow k$-perm
				\EndFor
			\EndFor
		\EndProcedure
		\algstore{odino}
	\end{algorithmic}
	\caption{\textsc{ODin0}}
	\label{alg:odino}
\end{algorithm}

\begin{algorithm}[!Ht]
	\begin{algorithmic}[1]
		\algrestore{odino}
		\Procedure{checkOrderDep}{$comb$}
			\If{$lhs$ of $comb$ is constant}
				\If{$rhs$ of $comb$ is constant}
					\State $\Call{applyInference}{lhs, rhs}$
					\State $\Call{applyInference}{rhs, lhs}$
				\EndIf
			\Else
				\If{$rhs$ of $comb$ is constant}
					\State $\Call{applyInference}{lhs, rhs}$
				\Else
					\If{$comb$ is an order dependency}
						\State $\Call{applyInference}{lhs, rhs}$
						\If{$rhs$ has a unique column}
							\State $\Call{applyInference}{rhs, lhs}$
							\State $\Call{applyInference}{rhs, lhs \cup rhs}$
						\EndIf
					\EndIf
				\EndIf
			\EndIf
		\EndProcedure
		\Statex
		\Procedure{applyInference}{$lhs, rhs$}
			\State $\mathfrak{O}.add(lhs \Rightarrow rhs)$
			\State $\mathfrak{O}.add(lhs \Rightarrow rhs \cup lhs)$
			\State $\mathfrak{O}.add(lhs \cup rhs \Rightarrow rhs)$
			\State $\mathfrak{O}.add(lhs \cup rhs \Rightarrow rhs \cup lhs)$
			\State $\mathfrak{O}.add(rhs \cup lhs \Rightarrow lhs \cup rhs)$
			\State $\mathfrak{O}.add(rhs \cup lhs \Rightarrow lhs)$
		\EndProcedure
		\Statex
		\Procedure{augmentNode}{$node, columns$}
			\ForAll{$c \in columns \setminus node.cols$}
				\State $n \gets Node(node.cols \cup \{c\}, node.combs)$
				\State $lattice.add(n)$
			\EndFor
		\EndProcedure
	\end{algorithmic}
\end{algorithm}

\subsection{Validation}\label{subsec:validation}
The set of inference rules presented in \cite{szlichta2012} is proven to be sound, that is, they lead to true conclusions. We take advantage of these axioms to prune the searching space, applying them whenever we find a valid order dependency. 
In particular, we use the following rules and related theorems:

\begin{rules}[Reflexivity]\label{rule:ref}
$\forall \mathbf{X}, \mathbf{Y} \quad \mathit{\mathbf{X}\mathbf{Y}} \mapsto_\leq \mathbf{X}$
\end{rules}

\begin{rules}[Prefix]\label{rule:pref}
$\forall \mathbf{X}, \mathbf{Y}, \mathbf{Z} \quad \mathbf{X} \mapsto_\leq \mathbf{Y} \Rightarrow \mathit{\mathbf{Z}\mathbf{X}} \mapsto_\leq \mathit{\mathbf{Z}\mathbf{Y}}$
\end{rules}

\begin{rules}[Normalization]\label{rule:norm}
$\forall \mathbf{X}, \mathbf{Y}, \mathbf{W}, \mathbf{V} \quad \mathit{\mathbf{W}\mathbf{X}\mathbf{Y}\mathbf{X}\mathbf{V}} \leftrightarrow \mathit{\mathbf{W}\mathbf{X}\mathbf{Y}\mathbf{V}}$
\end{rules} 

\begin{rules}[Transitivity]\label{rule:trans}
$\forall \mathbf{X}, \mathbf{Y}, \mathbf{Z} \quad \mathbf{X} \mapsto_\leq \mathbf{Y} \wedge$ $\mathbf{Y} \mapsto_\leq \mathbf{Z} \Rightarrow \mathbf{X} \mapsto_\leq \mathbf{Z}$
\end{rules}

\begin{rules}[Suffix]\label{rule:suf}
$\mathbf{X} \mapsto_\leq \mathbf{Y} \Rightarrow \mathbf{X} \leftrightarrow \mathit{\mathbf{Y}\mathbf{X}}$
\end{rules}

\begin{theorem}[Augmentation]\label{rule:augm}
Let $\mathbf{X}$, $\mathbf{Y}$, and $\mathbf{Z}$ be lists of attributes. Then, $\mathbf{X} \mapsto_\leq \mathbf{Y} \Rightarrow \mathbf{X}\mathbf{Z} \mapsto_\leq \mathbf{Y}$.
\end{theorem}
\vspace{-18pt}
\begin{proof}
$\mathbf{X} \mapsto_\leq \mathbf{X} \xRightarrow{Rule~\ref{rule:ref}} \mathbf{X}\mathbf{Z} \mapsto_\leq \mathbf{X} \xRightarrow{Rule~\ref{rule:trans}} \mathbf{X}\mathbf{Z} \mapsto_\leq \mathbf{Y}$
\end{proof}

\begin{theorem}[Left Eliminate]\label{rule:lefteli}
Let $\mathbf{X}$, $\mathbf{Y}$, $\mathbf{Z}$, and $\mathbf{V}$ be lists of attributes. Then, $\mathbf{X} \mapsto_\leq \mathbf{Y} \Rightarrow \mathbf{V}\mathbf{Y}\mathbf{X}\mathbf{Z} \leftrightarrow \mathbf{V}\mathbf{X}\mathbf{Z}$.
\end{theorem}
\vspace{-18pt}
\begin{proof}
See proof of Theorem 8 \cite{szlichta2012}.
\end{proof}

\begin{theorem}[Correlation between $\mapsto_\leq$, $\mapsto_<$]\label{th:corr}
Let $\mathbf{X}$, $\mathbf{Y}$, and $\mathbf{Z}$ be lists of attributes. Then, $\mathbf{X} \mapsto_\leq \mathbf{Y} \Rightarrow \mathbf{Y} \mapsto_< \mathbf{X}$.
\end{theorem}
\vspace{-18pt}
\begin{proof}[Proof by Contradiction]
Assume that $\mathbf{X} \mapsto_\leq \mathbf{Y}$ holds and suppose $\mathbf{Y} \nmapsto_< \mathbf{X}$. By definition of order dependency over operator $\preceq$, there exists  $t, s \in \mathbf{r}$ such that $t_\mathbf{Y} \prec s_\mathbf{Y} \wedge t_\mathbf{X} \succeq s_\mathbf{X}$. By validity of $\mathbf{X} \mapsto_\leq \mathbf{Y}$, it follows that $s_\mathbf{X} \preceq t_\mathbf{X} \Rightarrow s_\mathbf{Y} \preceq t_\mathbf{Y}$. We reach an absurdum, hence we conclude  $\mathbf{Y} \mapsto_< \mathbf{X}$ holds.
\end{proof}

\begin{corollary}\label{cor:unique}
If $\mathbf{Y}$ is unique, then $\mathbf{X} \mapsto_\leq \mathbf{Y} \Rightarrow \mathbf{Y} \mapsto_\leq \mathbf{X}$.
\end{corollary}
\vspace{-18pt}
\begin{proof}
By Theorem~\ref{th:corr} $\mathbf{Y} \mapsto_< \mathbf{X}$, thus $\forall s,t \in \mathbf{r} \quad s_\mathbf{Y} \prec t_\mathbf{Y} \Rightarrow s_\mathbf{X} \prec t_\mathbf{X}$. The relationship $\mapsto_\leq$ holds iff $\forall u, v \in \mathbf{r}. u_\mathbf{Y} = v_\mathbf{Y} \Rightarrow u_\mathbf{X} \preceq t_\mathbf{X}$. Since $\mathbf{Y}$ is unique, the premise $u_\mathbf{Y} = v_\mathbf{Y}$ is false, hence the implication is true.
\end{proof}
\vspace{-9pt}
Given two attribute lists $\mathbf{X}$ and $\mathbf{Y}$ we can build 16 candidate rules, obtained by computing the valid $k$-permutations with repetition from the set $\{\mathbf{X}, \mathbf{Y}\}$. A $k$-permutation $P$ is \emph{valid} if it can be split into two lists $lhs$, $rhs$ that do not contain repetitions of any attribute list.

Not all the candidate rules are worthy to be examined. Indeed, there are trivial dependencies that hold always:

\begin{compactitem}
\item[\textbf{OD1}] $\mathbf{X} \mapsto_\leq \mathbf{X}$
\item[\textbf{OD2}] $\mathbf{Y} \mapsto_\leq \mathbf{Y}$
\item[\textbf{OD3}] $\mathbf{XY} \mapsto_\leq \mathbf{XY}$
\item[\textbf{OD4}] $\mathbf{YX} \mapsto_\leq \mathbf{YX}$
\item[]
\item[\textbf{OD5}] $\mathbf{XY} \mapsto_\leq \mathbf{X}$ (by Rule~\ref{rule:ref})
\item[\textbf{OD6}] $\mathbf{YX} \mapsto_\leq \mathbf{Y}$ (by Rule~\ref{rule:ref})
\end{compactitem}

The remaining 10 rules express useful semantic information, thus they should all be investigated. However, from the validity of a rule we can infer the validity of other dependencies by applying the axioms above. In particular, if 
 $$\textbf{OD7}\;\; \mathit{\mathbf{X}} \mapsto_\leq \mathit{\mathbf{Y}}$$ holds, then also the following hold:

\begin{compactitem}
\item[\textbf{OD8}] $\mathbf{X} \mapsto_\leq \mathbf{YX}$ (by Rule~\ref{rule:suf})
\item[\textbf{OD9}] $\mathbf{X} \mapsto_\leq \mathbf{XY}$ (by Rule~\ref{rule:pref} + Rule~\ref{rule:norm})
\item[\textbf{OD10}] $\mathbf{YX} \mapsto_\leq \mathbf{X}$ (by Rule~\ref{rule:suf})
\item[\textbf{OD11}] $\mathbf{XY} \mapsto_\leq \mathbf{Y}$ (by Rule~\ref{rule:augm})
\item[\textbf{OD12}] $\mathbf{XY} \mapsto_\leq \mathbf{YX}$ (by \textbf{OD11} + Rule~\ref{rule:suf} + Rule~\ref{rule:norm})
\item[\textbf{OD13}] $\mathbf{YX} \mapsto_\leq \mathbf{XY}$ (by \textbf{OD11} + Rule~\ref{rule:suf} + Rule~\ref{rule:norm})
\end{compactitem}

Therefore, thanks to the inference rules, we must check only three ODs:

\begin{compactitem}
\item[\textbf{OD14}] $\mathbf{Y} \mapsto_\leq \mathbf{X}$
\item[\textbf{OD15}] $\mathbf{Y} \mapsto_\leq \mathbf{XY}$
\item[\textbf{OD16}] $\mathbf{Y} \mapsto_\leq \mathbf{YX}$
\end{compactitem}

Notice that, under the right conditions, Corollary~\ref{cor:unique} ensures that to prune also \textbf{OD14}, hence proving the validity of both \textbf{OD15} (by Rule~\ref{rule:pref} + Rule~\ref{rule:norm}) and \textbf{OD16} (by Rule~\ref{rule:suf}). 

In addition, Rule~\ref{rule:pref} allows to prune specific rules containing an extra attribute list $\mathbf{Z}$:
\begin{compactitem}
\item $\mathbf{ZX} \mapsto_\leq \mathbf{ZY}$
\item $\mathbf{ZX} \mapsto_\leq \mathbf{ZXY}$
\item $\mathbf{ZX} \mapsto_\leq \mathbf{ZYX}$
\item $\mathbf{ZYX} \mapsto_\leq \mathbf{ZX}$
\item $\mathbf{ZXY} \mapsto_\leq \mathbf{ZYX}$
\item $\mathbf{ZYX} \mapsto_\leq \mathbf{ZXY}$
\item $\mathbf{ZXY} \mapsto_\leq \mathbf{ZY}$
\end{compactitem}

On the other hand, if $\mathit{\mathbf{X}} \mapsto_\leq \mathit{\mathbf{Y}}$ does not hold, the pruning is minor. Indeed, only the following can be discarded because surely false:

\begin{compactitem}
\item $\mathbf{X} \nmapsto_\leq \mathbf{YX}$ (trivial consequence)
\item $\mathbf{XY} \nmapsto_\leq \mathbf{YX}$
\item $\mathbf{YX} \nmapsto_\leq \mathbf{XY}$
\end{compactitem}  

The following proposition lets us remove the latter two from the candidates; moreover it ensures that when $\mathbf{X} \mapsto_\leq \mathbf{Y}$ holds, so do \textbf{OD12} and \textbf{OD13}, therefore it justifies the strategy of \textsc{ODin0} of never examining \textbf{OD12} and \textbf{OD13}:

\begin{proposition}[Equivalence of \textbf{OD11}, \textbf{OD12} and \textbf{OD13}]\label{prop:equiv}
$\mathbf{X}\mathbf{Y} \mapsto_\leq \mathbf{Y} \Leftrightarrow \mathbf{X}\mathbf{Y} \mapsto_\leq \mathbf{Y}\mathbf{X} \Leftrightarrow \mathbf{Y}\mathbf{X} \mapsto_\leq \mathbf{X}\mathbf{Y}$.
\end{proposition}
\vspace{-18pt}
\begin{proof}[OD11 $\Rightarrow$ OD12]
It follows from the application of Rule~\ref{rule:suf} and Rule~\ref{rule:norm}.
\end{proof}
\vspace{-18pt}
\begin{proof}[OD11 $\Leftarrow$ OD12]
We prove the converse. $\mathbf{X}\mathbf{Y} \nmapsto_\leq \mathbf{Y} \Rightarrow \exists s, t \in \mathbf{r}. s_{\mathbf{X}\mathbf{Y}} \preceq t_{\mathbf{X}\mathbf{Y}} \wedge t_\mathbf{Y} \prec s_\mathbf{Y} \Rightarrow t_{\mathbf{Y}\mathbf{X}} \prec s_{\mathbf{Y}\mathbf{X}} \Rightarrow \mathbf{X}\mathbf{Y} \nmapsto_\leq \mathbf{Y}\mathbf{X}$.
\end{proof}
\vspace{-18pt}
\begin{proof}[OD12 $\Rightarrow$ OD13]
$\mathit{\mathbf{X}\mathbf{Y}} \mapsto_\leq \mathit{\mathbf{Y}\mathbf{X}}$
$\xRightarrow{Rule~\ref{rule:pref}}$ $\mathit{\mathbf{Y}\mathbf{X}\mathbf{Y}} \mapsto_\leq \mathit{\mathbf{Y}\mathbf{Y}\mathbf{X}}$\\
$\xRightarrow{Rule~\ref{rule:suf}}$ $\mathit{\mathbf{Y}\mathbf{X}\mathbf{Y}} \mapsto_\leq \mathit{\mathbf{Y}\mathbf{Y}\mathbf{X}\mathbf{Y}\mathbf{X}\mathbf{Y}}$
$\xRightarrow{Rule~\ref{rule:norm}}$ $\mathit{\mathbf{Y}\mathbf{X}} \mapsto_\leq \mathit{\mathbf{Y}\mathbf{X}\mathbf{X}\mathbf{Y}}$\\
$\xRightarrow{Rule~\ref{rule:lefteli}}$ $\mathit{\mathbf{Y}\mathbf{X}} \mapsto_\leq \mathit{\mathbf{X}\mathbf{Y}}$
\end{proof}
\vspace{-18pt}
\begin{proof}[OD12 $\Leftarrow$ OD13]
The proof is the same as the previous one.
\end{proof}

Additional pruning can be carried out when the values of an attribute list are all equal:

\begin{observation}
Let $A$, $C$ be lists of attributes. If $\forall s, t \in \mathbf{r}$ $s_A = t_A$, then the order dependency $C \mapsto_\leq A$ holds. On the other hand, if $\exists s, t \in \mathbf{r}. s_C \prec t_C$, then $A \nmapsto_\leq C$.
\end{observation}

\subsection{Complexity}
Let $\mathbf{r}$ be a relation with attributes $A_1, \dots, A_n$. \textsc{ODin0} grows a lattice of $n$ levels, where each level $k$ contains $\binom{n}{k}$ nodes, for a total of $\sum_{k=1}^{n}{\binom{n}{k}}=2^n$. Notice that, adding one attribute doubles the number of nodes in the lattice, thus the algorithm must examine twice as many elements than before.
Procedure~\textsc{initialize} generates the potential order dependencies of a given node, therefore its complexity is also determined by $n$. In particular, let $\mathfrak{C}$ be the set of candidate rules that can be constructed with $n$ attributes. Both the lhs and the rhs of a rule can be any $k$-permutation of $A_1, \dots, A_n$, therefore the cardinality of $\mathfrak{C}$ is $$|\mathfrak{C}| = \sum\limits_{k=1}^{n}{\frac{n!}{(n-k)!}} \cdot \sum\limits_{m=1}^{n}{\frac{n!}{(n-m)!}} = \left(\frac{n(n^n-1)}{n-1}\right)^2$$
\vspace{-9pt}

On the other hand, the number of rows $m$ slightly affects the performance in comparison to the number of columns. Indeed, Procedure~\textsc{checkOrderDep} scans $\mathbf{r}$ once to check if the given dependency holds, thus it requires linear time $O(m)$. 
Also \textsc{augmentNode} can be done in linear time $O(n)$, whereas \textsc{applyInference} takes constant time $O(1)$. In total, the complexity bound is $O(2^n(n + m \cdot n^{2n}))$.