%!TEX root=../bdp.tex
\label{s_background}

In this section we review the definition of ODs and we introduce some theorems
that will produce pruning rules for our algorithm.

\subsection{Definitions and Notational Conventions}\label{ss_definitions}

We adopt the notational conventions of \cite{szlichta:2012:fundamentals},
summarized in Table \ref{t_notation}.

Let $\Rel$ be a relation over the set of attributes $\Attrs$ and $\rel$ be a
table instance of $\Rel$. 
We can assume that $\rel$ has a unique column $\textit{id}$ such that $\rel$
can be represented, without loss of generality, as a \emph{set} of 
tuples\footnote{As the name suggests, the role of $\textit{id}$ is to provide
an index. If $\rel$ has no unique columns, an additional column enumerating 
the tuples in $\rel$ can be introduced without loss of generality. $\rel$ 
can thus always be treated as a \emph{set} of tuples instead of a multi-set.}.

\begin{table}
  \begin{tabular}{|lp{0.9\linewidth}|}
  \hline
  \multicolumn{2}{|l|}{Relations:} \\
  $\blacktriangleright$ & $\Rel$, written as a capital letter in bold italics, is a \emph{relation}
                          over a set of attributes $\Attrs$; \\
  $\blacktriangleright$ & $\rel$, written as a lowercase letter in bold italics, is a \emph{table instance}
                          over $\Rel$, i.e. a \emph{set of tuples}; \\
  $\blacktriangleright$ & Single \emph{attributes} are represented with capital letters: $A, B, C$; \\
  $\blacktriangleright$ & \emph{Tuples} are represented with lowercase letters: $p, q, s, t$. \\

  \multicolumn{2}{|l|}{Sets:} \\
  $\blacktriangleright$ & \emph{Sets} of attributes are indicated with calligraphic letters  $\mathcal{X}, \mathcal{Y}, \mathcal{Z}$; \\
  $\blacktriangleright$ & $\mathcal{X}\mathcal{Y}$ is a shorthand for $\mathcal{X} \cup \mathcal{Y}$ 
                          and $\mathcal{X}\mathcal{A}$ is a shorthand for $\mathcal{X} \cup \{A\}$; \\

  \multicolumn{2}{|l|}{Lists:} \\
  $\blacktriangleright$ & Bold capital letters are \emph{lists} of attributes: $\X, \Y, \Z$.
                          they can represent the empty list $[\cdot]$; \\
  $\blacktriangleright$ & A list is denoted with square brackets $[A, B, C]$.
                          A list $[A|\T]$ is composed by an \emph{head} $A$ and a \emph{tail} $\T$; \\
  $\blacktriangleright$ & $\X\Y$ is a shorthand for $\X \circ \Y$,
                          $\X A$ and $A\X$ are shorthands for $\X \circ [A]$ and $[A] \circ \X$ respectively,
                          $AB$ denotes $[A, B]$; \\
  \hline
  \end{tabular}
\caption{\label{t_notation} Notational conventions (from \cite{szlichta:2012:fundamentals})}
\end{table}

We define order dependencies using the $\leqdep$ operator, this is equivalent to
ascending lexicographical ordering over list of attributes as expressed by the
\textsc{ORDER BY} clause in SQL, the definitions follows \cite{szlichta:2012:fundamentals}:
\begin{definition}[Operator $\leqdep$]\label{d_leqdep}
Given a list of attributes $\X := [A|\T]$, and a two tuples $p, q \in \rel$.
Operator $\leqdep$ is defined as follows:
\begin{equation}
  \begin{aligned}
    p_{\X} \leqdep q_{\X} && \text{ if }    & (p_{A} < q_{A}) \\
			  && \text{ or if } & (p_{A} = q_{A}) \text{ and }
					      (\T = [\cdot] \text{ or } p_{\T} \leqdep q_{\T})
  \end{aligned}
\end{equation}
\end{definition}
We can also define the operator $\ltdep$: $p_{\X} \ltdep q_{\X}$ iff 
$q_{\X} \npreccurlyeq p_{\X}$.

Without loss of generality we can assume that our data consist in integer numbers 
and the $\leq$ operator has the usual meaning. This correspond to having defined 
a \emph{comparison operator} $\leq$ on all data types encountered in $\rel$.

The comparison operator of definition \ref{d_leqdep} is necessary to check 
if a list of columns is ordered, with this we can introduce the definition
of \emph{order dependency}:
\begin{definition}[Order dependency \cite{szlichta:2012:fundamentals}]
Let $\X$ and $\Y$ be two lists of attributes. $\X \dep \Y$ is an \emph{order dependency}
over the relation $\Rel$ if, for every pair of \emph{admissible} tuples $p, q$ in a
relation instance $\rel$ over $\Rel$ the following implication holds:
  \begin{equation}
    p_{\X} \leqdep q_{\X} \Rightarrow p_{\Y} \leqdep q_{\Y}
  \end{equation}
\end{definition}

If both $\X \dep \Y$ and $\Y \dep \X$ hold then we write $\X \ddep \Y$. Attributes $\X$
and $\Y$ are said to be \emph{order equivalent} if $\X\Y \ddep \Y\X$ and are denoted
with $\X \sim \Y$.

In the previous definition we used the operator $\leqdep$, order dependencies can 
be defined in an analogous way using the operator $\ltdep$. The following theorem
establish the relation between the two definitions{\footnote{In the following theorem we will use the symbols $\depLeq$ and
$\depLt$ to make explicit that we are using the definition of order dependency
with operators $\leqdep$ and $\ltdep$ respectively.}}:
\begin{theorem}[Correspondence of OD with $\leqdep$ and $\ltdep$]
  \begin{equation}
    \infer{
      {\begin{array}{l}
	\Y \depLt \X
      \end{array}}    
    }{
      {\begin{array}{l}
	\X \depLeq \Y
      \end{array}}    
    }
  \end{equation}
\end{theorem}

Order dependencies satisfy a set axioms $\J$, introduced by Szlichta in 
\cite{szlichta:2012:fundamentals}, that are reported in Table \ref{t_axioms}.
These axioms are analogous to the Armstrong axioms for functional dependencies.

\begin{table}[h]
\centering
  \begin{tabular}{ll}
    \textbf{OD1:} \emph{Reflexivity} & \textbf{OD2:} \emph{Prefix} \\
    \begin{minipage}[t]{0.4\linewidth}
      $\X\Y \ndep \X$
    \end{minipage} & 
    \begin{minipage}{0.4\linewidth}
      $\infer{\Z\X \dep \Z\Y}{\X \dep \Y}$
    \end{minipage}\\

    \textbf{OD3:} \emph{Normalization} & \textbf{OD6:} \emph{Chain} \\
    \begin{minipage}[t]{0.4\linewidth}
      $\W\X\Y\X\V \ddep \W\X\Y\V$
    \end{minipage} &
    \multirow{5}{*}{\begin{minipage}{0.4\linewidth}
      \begin{equation*}
	\infer{
	{\begin{array}{l}
	  \X \sim \Z
	\end{array}}    
      }{
	{\begin{array}{l}
	 \begin{aligned}
	  & \X \sim \Y_{1} \\ 
	  \forall_{i \in [1, n-1]} & \X \sim \Y_{i+1} \\
	  & \Y_{n} \sim \Z\\
	  \forall_{i \in [1, n]} & \Y_{i}\X \sim \Y_{i}\Z\\
	\end{aligned}
	\end{array}}    
      }
      \end{equation*}
    \end{minipage}} \\

    \textbf{OD5:} \emph{Suffix} \\
    \begin{minipage}{0.4\linewidth}
	$\infer{\X \ddep \Y\X}{\X \dep \Y}$
    \end{minipage} \\
    \textbf{OD5:} \emph{Suffix} \\
    \begin{minipage}{0.4\linewidth}
	$\infer{\X \ddep \Y\X}{\X \dep \Y}$
    \end{minipage}
  \end{tabular}
\caption{\label{t_axioms} Axioms for order dependencies}
\end{table}

A complete algorithm must find at least all \emph{minimal} order dependencies over
an instance $\rel$ of a relation $\Rel$.
To introduce the concept of minimality for order dependencies we need to define
what is the \emph{closure} of a set of order dependencies and when two sets of
order dependencies are \emph{equivalent}.

The closure of the set of OD $\M$ denoted $\Mclos$
to be the set of ODs that are logically implied
by $M$ .
\begin{definition}[Closure]
Let $\J = {OD1 - OD6}$ from Table \ref{t_axioms} the closure of the
set of order dependencies $\M$ using the axioms $\J$ is
$\Mclos = \{\X \dep \Y | \M \vdash_{\J} \X \dep \Y \}$
\end{definition}

If two different sets of order dependencies have the same closure
then they are equivalent.
\begin{definition}[Equivalence of sets of ODs]
Let $\M_{1}$ and $\M_{2}$ be two sets of order dependencies.
$\M_{1}$ is equivalent to $\M_{2}$ \emph{iff}
\[\M_{1}^{+} = \M_{2}^{+}= \{\X \dep \Y | \M_{1(2)} \vdash_{\J} \X \dep \Y \}\]
\end{definition}

Minimality can be defined with respect to a single order dependency and within
a set of order dependencies.

A single order dependency is \emph{minimal} if the attribute list appearing
in it have no embedded order dependency, i.e. the list of attributes
is the shortest possible.
\begin{definition}[Minimal attribute list]
An attribute list $\X$ is minimal if there is no other list $\X'$ such that
$|\X'| < |\X|$ and $\X' \ddep \X$.
\end{definition}

An order dependency is minimal if both sides are composed  a minimal
attribute lists:
\begin{definition}[Minimal OD]
An OD $\X \dep \Y$ is minimal iff $\X \dep \Y$ and both $\X$ and $\Y$
are minimal attribute lists.
\end{definition}

A set of order dependencies is minimal if it is composed by the minimum 
number of minimal order dependencies such that all the order dependencies
that hold over an instance $\rel$ can be subsumed by it.
\begin{definition}[Minimal set of order dependencies]
Let $\M$ be the set of all the valid order dependencies over an instance 
$\rel$ of a relation $\Rel$, by definition $\Mclos = \M$.
A set of order dependencies $\Ss$ is \emph{minimal} if:
\begin{itemize}
 \item all order dependencies $\X \dep \Y \in \Ss$ are minimal;
 \item $\Ss^{+} = \M$;
 \item there is no minimal $\Ss'$ such that $|\Ss'| < |\Ss|$.
\end{itemize}
\end{definition}
In a minimal set of order dependencies if we remove an element $\X \dep \Y$
then $\Ss$ is unable to generate all the valid OD over $\rel$.

\subsection{Dimension of the Search Space}\label{ss_dimension_search_space}

The number of possibly valid ODs over a relation $\Rel$ is much more vast 
than the number of FDs: in fact if $\Rel$ has $n$ attributes then all
$k-$permutations of $n$ elements, that is all permutations of 
length $k$ of $n$ elements, must be considered both for the
left hand side (\emph{lhs}) and the right hand side (\emph{rhs}) of the candidate
ODs. In contrast with FDs there are non-trivial OD where the 
same attribute appear on the \emph{lhs} and the \emph{rhs} of a an OD, i.e.
$\X \dep \Y$ and $\Xx \cap \Yy \neq \emptyset$.

For example, consider the examples in table , in
\ref{t_repeat}.(a) we have that \textbf{$AB \ndep B$}, instead
in \ref{t_repeat}.(b) we have $AB \dep B$. For both tables 
the dependencies $A \ndep B$ and $B \ndep A$ hold.

\begin{table}[h]
\centering
    \begin{tabular}{c}
       \\
      $t_1$ \\
      $t_2$ \\
      $t_3$ \\
      $t_4$ \\
      $t_5$ \\
    \end{tabular}
\quad
  \subfloat[a][]{
    \begin{tabular}{|c|c|}
      \hline
      \textbf{A} & \textbf{B} \\ \hline
      1 & 4 \\ \hline
      2 & 5 \\ \hline
      3 & 6 \\ \hline
      3 & 7 \\ \hline
      4 & 1 \\ \hline
    \end{tabular}}
\qquad
  \subfloat[b][]{
    \begin{tabular}{|c|c|}
      \hline
      \textbf{A} & \textbf{B} \\ \hline
      1 & 4 \\ \hline
      2 & 5 \\ \hline
      3 & 6 \\ \hline
      3 & 7 \\ \hline
      4 & 7 \\ \hline
    \end{tabular}}
\caption{\label{t_repeat} In these two tables the OD $A \ndep B$ and $B \ndep A$ hold, but
in (a) $AB \ndep B$ and in (b) $AB \dep B$.}
\end{table}

Finding all valid order dependencies thus requires, in principle, 
the need for checking all combinations $\X \dep \Y$
where both $\X$ and $\Y$ can be permutations of length $k$ of the $n$ 
attributes in $\Rel$ with $1 \leq k \leq n$. 
If we denote with $S(n)$ the number of $k$-permutations of $n$ elements we 
have (a derivation is described in appendix \ref{a_nperm}):
\[ S(n) = \lfloor {e \cdot n!} \rfloor - 1\]

Thus, excluding trivial ODs of the form $\X \dep \X$ the number of candidates that needs
to be checked is:
\begin{equation}\label{f_ncomb}
  \begin{aligned}
    C(n) &= (S(n) - 1) \cdot (S(n) - 1) - (S(n) - 1) =\\
	 &= (\lfloor {e \cdot n!} \rfloor - 1) \cdot (\lfloor {e \cdot n!} \rfloor - 2) \\
	 &\propto \mathcal{O}((n!)^2)
  \end{aligned}
\end{equation}

With $10$ attributes there are more than $97$ trillion candidates ODs. 
For this reason we need to employ aggressive pruning rules to avoid
generating unneeded combinations.
