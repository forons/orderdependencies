%!TEX root=../bdp.tex
\label{s_introduction}

Discovering relations in data is a useful task with many  applications
such as database design, data profiling, data quality management,
and knowledge representation. Relationships in data can follow from the
semantics of the data or domain-specific properties that may be known 
in advance or hidden. 

Several types of dependencies can be found in relational data such as
\emph{functional dependencies} (FDs), which represent value consistency
among sets  of attributes, and \emph{inclusion dependencies} (INDs),
which represent value reference relationship \cite{liu:2010:discover}.

\begin{table}[h]
\centering
  \begin{tabular}{|l|r|r|r|}
    \hline
    \multicolumn{1}{|c|}{\textbf{name}} &
      \multicolumn{1}{|c|}{\textbf{income}} & \multicolumn{1}{c}{\textbf{bracket}} &
 	\multicolumn{1}{|c|}{\textbf{tax amount}} \\ \hline
    Thomas Anderson & 35,000 & 1 & 5,250 \\ \hline
    John Smith & 40,000 & 1 & 6,000 \\ \hline
    Joe Black & 60,000 & 2 & 9,500 \\ \hline
    Janet Darrel & 80,000 & 3 & 14,000 \\ \hline
  \end{tabular}
\caption{\label{t_example_tax} A table of relation data about income and taxes.}
\end{table}

For example, Table \ref{t_example_tax} contains data about taxes and yearly income:
$\texttt{name}$, (yearly) $\texttt{income}$, (tax) $\texttt{braket}$, $\texttt{tax\_amount}$.
We assume for that we have a progressive tax system with three
brackets $1$, $ 2$ and $3$: bracket $1$ for incomes in the range $[0,50,000\$]$, 
bracket $2$ for income in the range $(50,000\$-75,000\$]$ and bracket $3$ for 
income greater than $75,000\$$. The tax rate for the three brackets is $15, 20$ and 
$30\%$ respectively.
The direct proportionality between a person's $\texttt{income}$ 
and the amount of taxes she is going to pay can be encoded in the
dependencies satisfied by this relation: for example in this table we
can have that the following functional dependencies holds 
$\texttt{income} \fdep \texttt{tax\_amount}$, however there is a
stronger relationship called \emph{order dependencies} that can encode
better the semantics of the relation between the two aforementioned columns.

The concept underlying \emph{order dependencies} was introduced for 
the first time in the context of database systems in the early 1980s 
by Ginsburg and Hull \cite{ginsburg:1983:ordered, ginsburg:1983:order, ginsburg:1986:sort}.
In these early works the concept of \emph{pointwise order} was introduced:
pointwise ordering requires that a set of columns \emph{orders} another
set of columns. In example \ref{t_example_tax} columns 
$\texttt{income}, \texttt{tax\_amount} \pdep \texttt{braket}$ because
if either of the tuples 
$(\texttt{income}, \texttt{tax\_amount})$ or $(\texttt{tax\_amount}, \texttt{income})$ 
are lexicographically ordered then the column $\texttt{braket}$ is ordered in the
same way.

More recently, a new definition for order dependencies has been introduced
\cite{szlichta:2012:fundamentals} to represent an order preserving mapping
between \emph{lists} of attributes. In contrast with pointwise
the new definition distinguishes tuples with attributes in different order,
thus having lists of attributes instead of sets.

Referring to the previous example, the relationship between yearly $\texttt{income}$
and $\texttt{tax\_amount}$ or between $\texttt{tax\_amount}$ and tax $\texttt{braket}$
is stronger than a functional dependency, in particular it can be encoded 
as an order dependency. Order dependencies are stronger than functional dependencies
in that ODs subsume FDs \cite{szlichta:2012:fundamentals}.
In fact, in the example above $\texttt{tax\_amount}$ \emph{orders}  $\texttt{braket}$:
$\texttt{tax\_amount} \dep \texttt{braket}$\footnote{In the following we will use $\fdep$
to denote functional dependencies, and $\dep$ for order dependencies using
the lexicographical order. We use $\pdep$ to denote order dependencies where we use
a different definition of ordering, such as pointwise dependencies.}.

Discovered order dependencies can be exploited for query optimization: when order dependencies 
are known for a relation sorting operation can be sped up using indexes over attributes 
whose order implies the order on the requested attribute. 
Order dependencies are also exploited for query rewriting \cite{simmen:1996:fundamental, szlichta:2014b:business}
The \textsc{ORDER BY} clause can be rewritten in an analogous ways to what is 
done for functional dependencies and the \textsc{GROUP BY} clause.

If we perform the following query on the dataset represented in Table \ref{t_example_tax}:
\begin{verbatim}
  SELECT tax_amount, braket 
  FROM data
  ORDER BY tax_amount, braket
\end{verbatim}
the query optimizer can infer from the OD above that sorting the $\texttt{tax\_amount}$
columns already sorts also the bracket column, thus the \textsc{ORDER BY} can be simplified.

In general, dependencies can be extracted from application
requirements, from the context of queries or derived using from set of known
dependencies using inference rules. However, dependencies can also be discovered 
from data, and this process is called \emph{dependency discovery} \cite{liu:2010:discover}.

One of the fundamental differences between functional dependencies and order dependencies is
that for the latter the order of the attributes matters. Thus, the search space for
order dependencies is substantially wider than the one that detects functional dependencies. 
For this reason an algorithms for the detection of order dependencies have a greater complexity.

In this paper we propose an algorithm to perform \emph{order dependency discovery} on a
dataset. We utilize a bottom-up approach in the sense that the algorithm checks shorter
column lists at the beginning and progresses towards longer lists in subsequent stages.
The search space is completely covered, and pruning rules are applied to eliminate
candidates when the dependency (or lack thereof) can be inferred from dependencies
already discovered. We use aggressive pruning techniques to limit the number of
candidates to be checked.

The paper is organized as follows: section \ref{s_background} reviews the definition 
of ODs and introduces some theorems that are used later. Section \ref{s_body} introduces
our algorithm and discuss some challenge concerning the implementation. We test our implementation
with various publicly available datasets and discuss the experimental results in section
\ref{s_experiments}. Section \ref{s_conclusions} concludes the paper.
