package de.metanome.algorithms.odin0;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.AbstractMap.SimpleEntry;

public class Node {

	private byte[]																byteSet;
	private List<Byte>															byteSetList;
	private Map<List<Byte>, Map<List<Byte>, Set<Map.Entry<List<Byte>, Byte>>>>	providedKeySet;
	private Map<List<Byte>, Set<Map.Entry<List<Byte>, Byte>>>					mappa;
	private Set<List<Byte>>														combinations;
	private List<Byte>															support;

	public Node(List<Byte> byteSetList) {
		this.setKeySet(new HashMap<List<Byte>, Map<List<Byte>, Set<Map.Entry<List<Byte>, Byte>>>>());
		this.setMappa(new HashMap<List<Byte>, Set<Map.Entry<List<Byte>, Byte>>>());
		this.setByteSetList(byteSetList);
		this.byteSet = new byte[byteSetList.size()];
		for (int i = 0; i < byteSet.length; i++)
			this.byteSet[i] = byteSetList.get(i);
		this.setByteSet(this.byteSet);
	}

	public Node(byte[] byteSet) {
		this.setKeySet(new HashMap<List<Byte>, Map<List<Byte>, Set<Map.Entry<List<Byte>, Byte>>>>());
		this.setMappa(new HashMap<List<Byte>, Set<Map.Entry<List<Byte>, Byte>>>());
		this.setByteSet(byteSet);
		this.setByteSetList(new ArrayList<Byte>());
		for (byte b : byteSet)
			this.byteSetList.add(b);
		this.setByteSet(this.byteSet);
	}

	public void initialize() {
		combinations = new HashSet<List<Byte>>();
		augmentProvidedKeySet();
		byteSetPermutations();
		// printMap();
	}

	public void heapRecursive(int n, byte[] columns) {
		if (n == 1) {
			support = new ArrayList<Byte>();
			for (int k = 0; k < this.byteSet.length; k++)
				support.add(columns[k]);
			combinations.add(support);
			return;
		}
		int next = n - 1;
		for (int i = 0; i < n; i++) {
			heapRecursive(next, columns);
			swap(columns, n % 2 == 0 ? i : 0, next);
		}
	}

	private static void swap(byte[] data, int i, int j) {
		byte temp = data[i];
		data[i] = data[j];
		data[j] = temp;
	}

	public void byteSetPermutations() {
		int size = byteSet.length;
		if (size == 1)
			heapRecursive(byteSet.length, byteSet);
		Set<List<Byte>> keySet = mappa.keySet();
		int keySize;
		for (List<Byte> lComb : combinations) {
			mappa.put(lComb, new HashSet<Map.Entry<List<Byte>, Byte>>());
			for (List<Byte> key : keySet) {
				keySize = key.size();
				if (keySize != size) {
					if (lComb.subList(0, keySize).equals(key))
						// trivial, pruning of XY -> X [reflexive property]
						mappa.get(lComb).add(new SimpleEntry<List<Byte>, Byte>(key, (byte) 2));
					else
						mappa.get(lComb).add(new SimpleEntry<List<Byte>, Byte>(key, (byte) 0));
				}
			}
			for (List<Byte> rComb : combinations) {
				if (lComb.equals(rComb))
					// trivial, pruning of X -> X
					mappa.get(lComb).add(new SimpleEntry<List<Byte>, Byte>(rComb, (byte) 2));
				else if (checkSymmetric(size, lComb, rComb)) {
					// XY -> YX
					mappa.get(lComb).add(new SimpleEntry<List<Byte>, Byte>(rComb, (byte) 1));
				} else
					mappa.get(lComb).add(new SimpleEntry<List<Byte>, Byte>(rComb, (byte) 0));
			}
		}
	}

	private boolean checkSymmetric(int size, List<Byte> lComb, List<Byte> rComb) {
		for (int i = 0; i < size; i++)
			if (lComb.subList(i, size).equals(rComb.subList(0, size - i)) && rComb.subList(size - i, size).equals(lComb.subList(0, i)))
				return true;
		return false;
	}

	public void augmentProvidedKeySet() {
		List<Byte> givenSet;
		Map<List<Byte>, Set<Map.Entry<List<Byte>, Byte>>> givenMap;
		List<Byte> notPresent;
		for (Map.Entry<List<Byte>, Map<List<Byte>, Set<Map.Entry<List<Byte>, Byte>>>> entry : providedKeySet.entrySet()) {
			givenSet = entry.getKey();
			givenMap = entry.getValue();
			notPresent = new ArrayList<Byte>(byteSetList);
			notPresent.removeAll(givenSet);
			if (notPresent.size() != 1) {
				System.err.println("ERROR HERE!");
				System.out.println(byteSetList.toString());
				System.exit(1);
			}
			augment(givenMap, notPresent.get(0));
		}
	}

	public void augment(Map<List<Byte>, Set<Map.Entry<List<Byte>, Byte>>> map, Byte missing) {
		Set<Map.Entry<List<Byte>, Byte>> addingSet;
		List<Byte> addingList, addingKey;
		List<Byte> keys;
		List<Byte> listKey = null;
		Set<Map.Entry<List<Byte>, Byte>> values;
		Set<List<Byte>> distinctKeys;
		for (Map.Entry<List<Byte>, Set<Map.Entry<List<Byte>, Byte>>> entry : map.entrySet()) {
			addingSet = new HashSet<Map.Entry<List<Byte>, Byte>>();
			keys = entry.getKey();
			values = entry.getValue();
			distinctKeys = new HashSet<List<Byte>>();
			for (Map.Entry<List<Byte>, Byte> list : values) {
				listKey = list.getKey();
				distinctKeys.add(listKey);
				if (list.getValue() == 2) {
					addingList = new ArrayList<Byte>(missing);
					addingList.addAll(new ArrayList<Byte>(listKey));
					addingKey = new ArrayList<Byte>(missing);
					addingKey.addAll(new ArrayList<Byte>(keys));
					if (!mappa.containsKey(addingKey))
						mappa.put(addingKey, new HashSet<Map.Entry<List<Byte>, Byte>>());
					mappa.get(addingKey).add(new SimpleEntry<List<Byte>, Byte>(addingList, (byte) 2));
				}
				for (int i = 0; i < listKey.size(); i++) {
					addingList = new ArrayList<Byte>(listKey);
					addingList.add(i, missing);
					if (addingList.size() == byteSet.length)
						combinations.add(addingList);
					addingSet.add(new SimpleEntry<List<Byte>, Byte>(addingList, (byte) 0));
				}
				addingList = new ArrayList<Byte>(listKey);
				addingList.add(listKey.size(), missing);
				if (addingList.size() == byteSet.length)
					combinations.add(addingList);
				if (list.getValue() == 1)
					addingSet.add(new SimpleEntry<List<Byte>, Byte>(addingList, (byte) 1));
				else
					addingSet.add(new SimpleEntry<List<Byte>, Byte>(addingList, (byte) 0));
			}
			mappa.put(keys, addingSet);
		}
	}

	public void augment(List<Byte> columns) {
		List<Byte> lhs, rhs;
		int size = columns.size();
		for (int i = 1; i <= size; i++) {
			lhs = new ArrayList<>(columns.subList(0, i));
			rhs = new ArrayList<>(columns.subList(i, size));

			if (!mappa.containsKey(lhs))
				mappa.put(lhs, new HashSet<Map.Entry<List<Byte>, Byte>>());
			mappa.get(lhs).add(new SimpleEntry<List<Byte>, Byte>(rhs, (byte) 0));

			for (int l = 0; l < i; l++) {
				for (int r = 0; r <= size - i; r++) {
					List<Byte> toAdd = new ArrayList<Byte>(rhs);
					toAdd.add(r, columns.get(l));
					mappa.get(lhs).add(new SimpleEntry<List<Byte>, Byte>(toAdd, (byte) 0));
				}
			}
		}
	}

	public void printMap() {
		System.out.println("===========================");
		System.out.println("==========" + getByteSetList().toString() + "===========");
		System.out.println("===========================");
		for (Map.Entry<List<Byte>, Set<Map.Entry<List<Byte>, Byte>>> lhs : mappa.entrySet()) {
			List<Byte> lhsValue = lhs.getKey();
			for (Map.Entry<List<Byte>, Byte> rhs : lhs.getValue())
				System.out.println(lhsValue + "->" + rhs);
		}
	}

	public Set<List<Byte>> getCombinations() {
		return combinations;
	}

	public void setCombinations(Set<List<Byte>> combinations) {
		this.combinations = combinations;
	}

	public Map<List<Byte>, Map<List<Byte>, Set<Map.Entry<List<Byte>, Byte>>>> getKeySet() {
		return providedKeySet;
	}

	public void setKeySet(Map<List<Byte>, Map<List<Byte>, Set<Map.Entry<List<Byte>, Byte>>>> keySet) {
		this.providedKeySet = keySet;
	}

	public byte[] getByteSet() {
		return byteSet;
	}

	public void setByteSet(byte[] byteSet) {
		this.byteSet = byteSet;
	}

	public Map<List<Byte>, Set<Map.Entry<List<Byte>, Byte>>> getMappa() {
		return mappa;
	}

	public void setMappa(Map<List<Byte>, Set<Map.Entry<List<Byte>, Byte>>> mappa) {
		this.mappa = mappa;
	}

	public List<Byte> getByteSetList() {
		return byteSetList;
	}

	public void setByteSetList(List<Byte> byteSetList) {
		this.byteSetList = byteSetList;
	}

	public int returnIndexToInsertOrMinusOneIfAlreadyPresent(byte toInsert) {
		for (int i = 0; i < this.byteSetList.size(); i++) {
			if (this.byteSetList.get(i) == toInsert)
				return -1;
			if (this.byteSetList.get(i) > toInsert)
				return i;
		}
		return byteSet.length;
	}

	@Override
	public boolean equals(Object obj) {
		if (obj == null)
			return false;
		if (obj == this)
			return true;
		if (!(obj instanceof Node))
			return false;
		Node other = (Node) obj;
		if (this.byteSetList.size() == other.getByteSetList().size())
			if (this.byteSetList.equals(other.getByteSetList()))
				return true;
		return false;
	}
}