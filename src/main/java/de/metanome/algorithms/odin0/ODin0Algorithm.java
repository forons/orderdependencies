package de.metanome.algorithms.odin0;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.lang3.time.DateUtils;

import de.metanome.algorithm_integration.AlgorithmConfigurationException;
import de.metanome.algorithm_integration.AlgorithmExecutionException;
import de.metanome.algorithm_integration.ColumnIdentifier;
import de.metanome.algorithm_integration.ColumnPermutation;
import de.metanome.algorithm_integration.input.InputGenerationException;
import de.metanome.algorithm_integration.input.InputIterationException;
import de.metanome.algorithm_integration.input.RelationalInput;
import de.metanome.algorithm_integration.input.RelationalInputGenerator;
import de.metanome.algorithm_integration.result_receiver.CouldNotReceiveResultException;
import de.metanome.algorithm_integration.result_receiver.OrderDependencyResultReceiver;
import de.metanome.algorithm_integration.results.OrderDependency;
import de.metanome.algorithm_integration.results.OrderDependency.ComparisonOperator;
import de.metanome.algorithm_integration.results.OrderDependency.OrderType;
import de.metanome.algorithms.odin0.order.Column;
import de.metanome.algorithms.odin0.order.sorting.partitions.RowIndexedDateValue;
import de.metanome.algorithms.odin0.order.sorting.partitions.RowIndexedDoubleValue;
import de.metanome.algorithms.odin0.order.sorting.partitions.RowIndexedLongValue;
import de.metanome.algorithms.odin0.order.sorting.partitions.RowIndexedStringValue;
import de.metanome.algorithms.odin0.order.sorting.partitions.RowIndexedValue;
import de.metanome.algorithms.odin0.order.sorting.value.RowDateValue;
import de.metanome.algorithms.odin0.order.sorting.value.RowDoubleValue;
import de.metanome.algorithms.odin0.order.sorting.value.RowLongValue;
import de.metanome.algorithms.odin0.order.sorting.value.RowStringValue;
import de.metanome.algorithms.odin0.order.sorting.value.RowValue;
import de.metanome.algorithms.odin0.order.types.Datatype;
import de.metanome.algorithms.odin0.order.types.TypeInferrer;

public class ODin0Algorithm {

	protected RelationalInputGenerator		inputGenerator	= null;
	protected OrderDependencyResultReceiver	resultReceiver	= null;

	protected RelationalInput				input;
	protected int[]							columnIndices;

	protected String						tableName;
	protected int							numberOfColumns;

	List<Datatype>							types;

	protected String						relationName;
	protected List<String>					columnNames;

	// protected List<Byte> columnNamesByte;
	protected byte[]						columnByteIndices;

	// protected List<Map<RowIndexedValue, List<Long>>> invertedIndexMap;

	public int								numRows;

	protected List<Column>					dataByColumns;

	public void execute() throws AlgorithmExecutionException {
		// this.input = this.inputGenerator.generateNewCopy();

		// //////////////////////////////////////////
		// THE DISCOVERY ALGORITHM LIVES HERE :-) //
		// //////////////////////////////////////////
		long time = System.currentTimeMillis();
		try {
			try {
				this.initialize();
			} catch (final InputGenerationException i) {
				throw new AlgorithmConfigurationException("Could not initialize ORDER: " + i.getMessage(), i);
			}
			// To test if the algorithm gets data
			this.print();
			performWalk(columnByteIndices);
		} catch (OutOfMemoryError e) {
			e.printStackTrace();
		}
		this.printResult();
		// System.out.println(order.toString());

		// To test if the algorithm outputs results
		// this.outputSomething();

		time = System.currentTimeMillis() - time;
		// System.out.println("===================================");
		// System.out.println("===================================");
		System.out.println("Total time: " + time + "ms");
	}

	private void printResult() {
		System.out.println("===================================");
		System.out.println("===================================");
		// System.out.println(orderDependency.size() + " ORDER DEPENDENCY FOUND");
		System.out.println("===================================");
		System.out.println("===================================");
		// for (Map.Entry<List<Byte>, List<Byte>> entry : orderDependency)
		// System.out.println(entry.getKey().toString() + " => " + entry.getValue().toString());
		// System.out.println("===================================");
		// System.out.println("===================================");
	}

	protected void initialize() throws AlgorithmExecutionException {

		this.input = this.inputGenerator.generateNewCopy();
		this.tableName = this.input.relationName();

		this.columnNames = this.input.columnNames();
		this.numberOfColumns = this.input.numberOfColumns();
		System.out.println("NUMBER OF COLUMNS: " + numberOfColumns);
		this.columnByteIndices = new byte[this.numberOfColumns];
		this.columnIndices = new int[this.numberOfColumns];

		// get column indices
		// this.columnIndices = new int[this.numberOfColumns];
		for (int i = 0; i < this.numberOfColumns; i++) {
			this.columnIndices[i] = i;
			// this.columnNamesByte.set(i, (byte) i);
			this.columnByteIndices[i] = (byte) i;
		}

		long time = System.currentTimeMillis();
		this.inferTypes();
		this.loadData();

		time = System.currentTimeMillis() - time;
	}

	protected void inferTypes() throws InputGenerationException {
		final TypeInferrer inferrer = new TypeInferrer(this.inputGenerator);
		this.types = inferrer.inferTypes();
	}

	protected void loadData() throws InputIterationException, InputGenerationException {
		this.dataByColumns = new ArrayList<Column>();
		// this.invertedIndexMap = new ArrayList<Map<RowIndexedValue,
		// List<Long>>>();
		for (int i = 0; i < this.numberOfColumns; i++)
			this.dataByColumns.add(new Column());

		try (final RelationalInput input = this.inputGenerator.generateNewCopy()) {
			long tupleId = 0;
			while (this.input.hasNext()) {
				final List<String> currentRow = this.input.next();
				for (final int column : this.columnByteIndices) {
					this.storeData(tupleId, currentRow, column);
				}
				tupleId++;
				this.numRows++;
			}

			for (Column column : this.dataByColumns)
				column.endPreliminaries(numRows);
		} catch (final Exception e) {
			throw new InputGenerationException("Error while loading data.", e);
		}

		if (this.dataByColumns == null || this.dataByColumns.isEmpty()) {
			throw new InputGenerationException("Did not find any data in "
			// + this.tableName
					+ ".");
		}

	}

	private void storeData(final long tupleId, final List<String> currentRow, final int column) throws ParseException {

		final String stringValue = currentRow.get(column);
		RowIndexedValue riv;
		RowValue rv;
		switch (this.types.get(column).getSpecificType()) {
			case DOUBLE:
				final Double doubleValue = (stringValue == null) ? null : Double.parseDouble(stringValue);
				riv = new RowIndexedDoubleValue(tupleId, doubleValue);
				rv = new RowDoubleValue(doubleValue);
				break;
			case DATE:
				final Date dateValue = (stringValue == null) ? null : DateUtils.parseDate(stringValue, TypeInferrer.dateFormats);
				riv = new RowIndexedDateValue(tupleId, dateValue);
				rv = new RowDateValue(dateValue);
				break;
			case LONG:
				final Long longValue = (stringValue == null) ? null : Long.parseLong(stringValue);
				riv = new RowIndexedLongValue(tupleId, longValue);
				rv = new RowLongValue(longValue);
				break;
			case STRING:
				riv = new RowIndexedStringValue(tupleId, stringValue);
				rv = new RowStringValue(stringValue);
				break;
			default:
				riv = new RowIndexedStringValue(tupleId, stringValue);
				rv = new RowStringValue(stringValue);
				break;
		}
		this.dataByColumns.get(column).add(riv, rv, tupleId);
	}

	protected void print() throws InputGenerationException, InputIterationException {
		RelationalInput input = this.inputGenerator.generateNewCopy();
		this.relationName = input.relationName();
		// this.columnNames = input.columnNames();

		System.out.print(this.relationName);
		// System.out.print("( ");
		// System.out.print(StringUtils.join(columnNames, " | "));
		// System.out.println(")");
		// System.out.println("# COLUMNS " + dataByColumns.size());

		// while (input.hasNext()) {
		// System.out.print("| ");
		//
		// List<String> record = input.next();
		// for (String value : record)
		// System.out.print(value + " | ");
		//
		// System.out.println();
		// }

		System.out.println();
		System.out.println("===================================");
		System.out.println("===================================");
		System.out.println("============== START ==============");
		System.out.println("===================================");
		System.out.println("===================================");
		System.out.println();

		// for (int k = 0; k < dataByColumns.size(); k++) {
		// System.out.println("============ Column " + k + " =============");
		// if (checkUniquenessOfColumnAtIndex(k))
		// System.out.println("Column " + k + " is UNIQUE");
		// if (checkConstantnessOfColumnAtIndex(k))
		// System.out.println("Column " + k + " has the SAME VALUE");
		// sortColumnAtIndex(k);
		// System.out.println(this.invertedIndexMap.get(k).toString());

		// System.out.println();
		// }
	}

	private boolean checkOrderDependency(List<Byte> lhs, List<Byte> rhs) {

		List<RowIndexedValue> firstLeftTuple;
		List<RowIndexedValue> secondLeftTuple;
		List<RowIndexedValue> firstRightTuple;
		List<RowIndexedValue> secondRightTuple;
		int resLeftTuples, resRightTuples;
		for (int i = 0; i < numRows - 1; i++) {
			firstLeftTuple = new ArrayList<RowIndexedValue>();
			firstRightTuple = new ArrayList<RowIndexedValue>();
			for (Byte b : lhs)
				firstLeftTuple.add(dataByColumns.get(b).getData().get(i));
			for (Byte b : rhs)
				firstRightTuple.add(dataByColumns.get(b).getData().get(i));

			for (int j = i + 1; j < numRows; j++) {
				secondLeftTuple = new ArrayList<RowIndexedValue>();
				secondRightTuple = new ArrayList<RowIndexedValue>();
				for (Byte b : lhs)
					secondLeftTuple.add(dataByColumns.get(b).getData().get(j));
				for (Byte b : rhs)
					secondRightTuple.add(dataByColumns.get(b).getData().get(j));

				resLeftTuples = isLeftTupleMinorOrEqualThanRightTuple(firstLeftTuple, secondLeftTuple);
				resRightTuples = isLeftTupleMinorOrEqualThanRightTuple(firstRightTuple, secondRightTuple);

				if ((resLeftTuples == -1 && resRightTuples == 1) || (resLeftTuples == 0 && resRightTuples != 0) || (resLeftTuples == 1 && resRightTuples == -1))
					return false;
			}
		}
		return true;
	}

	public int isLeftTupleMinorOrEqualThanRightTuple(List<RowIndexedValue> firstTuple, List<RowIndexedValue> secondTuple) {
		for (int i = 0; i < firstTuple.size(); i++) {
			int res = firstTuple.get(i).compareTo(secondTuple.get(i));
			if (res < 0)
				return -1;
			else if (res > 0)
				return 1;
		}
		return 0;
	}

	// protected Set<Map.Entry<List<Byte>, List<Byte>>> orderDependency;

	private long getTimeForWhileApproach(byte[] columns) {
		long s = System.currentTimeMillis();
		// orderDependency = new HashSet<Map.Entry<List<Byte>, List<Byte>>>();
		int index;
		LinkedList<Node> queue = new LinkedList<Node>();
		Node node, newNode;
		Map<List<Byte>, Set<Map.Entry<List<Byte>, Byte>>> mappa;
		List<Byte> nodeByteSetList, newByteSetList, support;
		for (byte b : columns)
			queue.add(new Node(new byte[] { b }));

		boolean result;
		byte count;
		byte[] array1, array2;
		while (!queue.isEmpty()) {
			node = queue.poll();
			node.initialize();
			mappa = node.getMappa();
			for (Map.Entry<List<Byte>, Set<Map.Entry<List<Byte>, Byte>>> entry : mappa.entrySet()) {
				List<Byte> lhs = entry.getKey();
				Set<Map.Entry<List<Byte>, Byte>> rhss = entry.getValue();
				if (checkConstantness(lhs)) {
					for (Map.Entry<List<Byte>, Byte> rhsEntry : rhss) {
						List<Byte> rhs = rhsEntry.getKey();
						if (lhs.size() >= rhs.size())
							if (lhs.subList(0, rhs.size()).equals(rhs))
								rhsEntry.setValue((byte) 2);

						Byte flag = rhsEntry.getValue();
						if (flag.equals((byte) 0)) {
							if (checkConstantness(rhs)) {
								// right side constant implies the order dependency holds
								// System.out.println("PRINT 1: " + lhs + " => " + rhs);
								// System.out.println("PRINT 2: " + rhs + " => " + lhs);
								array1 = new byte[lhs.size()];
								for (int i = 0; i < lhs.size(); i++)
									array1[i] = lhs.get(i);

								array2 = new byte[rhs.size()];
								for (int i = 0; i < rhs.size(); i++)
									array2[i] = rhs.get(i);

								signalFoundOrderDependency(array1, array2, ComparisonOperator.SMALLER_EQUAL, OrderType.LEXICOGRAPHICAL);
								signalFoundOrderDependency(array2, array1, ComparisonOperator.SMALLER_EQUAL, OrderType.LEXICOGRAPHICAL);

								// orderDependency.add(new SimpleEntry<List<Byte>, List<Byte>>(lhs, rhs));
								// orderDependency.add(new SimpleEntry<List<Byte>, List<Byte>>(rhs, lhs));
								rhsEntry.setValue((byte) 2);
								for (Map.Entry<List<Byte>, Byte> lhsEntry : mappa.get(rhs)) {
									if (lhsEntry.getKey().equals(lhs))
										lhsEntry.setValue((byte) 2);
								}
								applyInference(lhs, rhs, mappa);
								applyInference(rhs, lhs, mappa);
							} else {

								// if right side is not constant and left side is constant, X -> Y does not hold, so prune also X -> YX
								support = new ArrayList<Byte>(rhs);
								support.addAll(lhs);
								for (Map.Entry<List<Byte>, Byte> rightLeftConcat : rhss)
									if (rightLeftConcat.getKey().equals(support)) {
										rightLeftConcat.setValue((byte) 1);
										break;
									}
							}
						}
					}
				} else {
					for (Map.Entry<List<Byte>, Byte> rhsEntry : rhss) {
						List<Byte> rhs = rhsEntry.getKey();
						if (lhs.size() >= rhs.size())
							if (lhs.subList(0, rhs.size()).equals(rhs))
								rhsEntry.setValue((byte) 2);

						Byte flag = rhsEntry.getValue();
						if (flag.equals((byte) 0)) {
							if (checkConstantness(rhs)) {
								// right side constant implies the order dependency holds
								// System.out.println("PRINT 3: " + lhs + " => " + rhs);

								array1 = new byte[lhs.size()];
								for (int i = 0; i < lhs.size(); i++)
									array1[i] = lhs.get(i);

								array2 = new byte[rhs.size()];
								for (int i = 0; i < rhs.size(); i++)
									array2[i] = rhs.get(i);

								signalFoundOrderDependency(array1, array2, ComparisonOperator.SMALLER_EQUAL, OrderType.LEXICOGRAPHICAL);

								// orderDependency.add(new SimpleEntry<List<Byte>, List<Byte>>(lhs, rhs));
								applyInference(lhs, rhs, mappa);
							} else {
								// left side has to be not constant, otherwise the order dependency does not hold.
								result = checkOrderDependency(lhs, rhs);
								// result = checkOrderDependencyWithInvertedIndex(lhs, rhs);
								if (result) {
									rhsEntry.setValue((byte) 2);
									if (anyColumnIsUnique(rhs)) {
										boolean areDisjoint = Collections.disjoint(lhs, rhs);
										support = new ArrayList<Byte>(lhs);
										support.addAll(rhs);
										count = areDisjoint ? (byte) 2 : (byte) 1;
										boolean foundLhs = false, foundSupport = false;
										for (Map.Entry<List<Byte>, Byte> leftUniqueAtRight : mappa.get(rhs)) {
											if (!foundLhs) {
												if (leftUniqueAtRight.getKey().equals(lhs)) {
													leftUniqueAtRight.setValue((byte) 2);
													count--;
													foundLhs = true;
													if (count <= 0)
														break;
												}
											}
											if (areDisjoint) {
												if (!foundSupport) {
													if (leftUniqueAtRight.getKey().equals(support)) {
														leftUniqueAtRight.setValue((byte) 2);
														// System.out.println("PRINT 4: " + lhs + " => " + support);
														array1 = new byte[rhs.size()];
														for (int i = 0; i < rhs.size(); i++)
															array1[i] = rhs.get(i);

														array2 = new byte[support.size()];
														for (int i = 0; i < support.size(); i++)
															array2[i] = support.get(i);

														signalFoundOrderDependency(array1, array2, ComparisonOperator.SMALLER_EQUAL, OrderType.LEXICOGRAPHICAL);
														// orderDependency.add(new SimpleEntry<List<Byte>, List<Byte>>(rhs, support));
														applyInference(rhs, support, mappa);
														count--;
														foundSupport = true;
														if (count <= 0)
															break;
													}
												}
											}
										}
										// System.out.println("PRINT 5: " + lhs + " => " + rhs);
										// orderDependency.add(new SimpleEntry<List<Byte>, List<Byte>>(rhs, lhs));
										// applyInference(rhs, lhs, mappa);
									}
									// System.out.println("PRINT 6: " + lhs + " => " + rhs);
									array1 = new byte[lhs.size()];
									for (int i = 0; i < lhs.size(); i++)
										array1[i] = lhs.get(i);

									array2 = new byte[rhs.size()];
									for (int i = 0; i < rhs.size(); i++)
										array2[i] = rhs.get(i);

									signalFoundOrderDependency(array1, array2, ComparisonOperator.SMALLER_EQUAL, OrderType.LEXICOGRAPHICAL);
									// orderDependency.add(new SimpleEntry<List<Byte>, List<Byte>>(lhs, rhs));
									applyInference(lhs, rhs, mappa);
								} else {
									// if X -> Y does not hold, prune also X -> YX
									rhsEntry.setValue((byte) 1);
									if (Collections.disjoint(lhs, rhs)) {
										support = new ArrayList<Byte>(rhs);
										support.addAll(lhs);
										for (Map.Entry<List<Byte>, Byte> rightLeftConcat : rhss)
											if (rightLeftConcat.getKey().equals(support)) {
												rightLeftConcat.setValue((byte) 1);
												break;
											}
									}
								}
							}
						}
					}
				}
			}
			nodeByteSetList = node.getByteSetList();
			for (byte b : columns) {
				index = node.returnIndexToInsertOrMinusOneIfAlreadyPresent(b);
				if (index != -1) {
					newByteSetList = new ArrayList<Byte>(nodeByteSetList);

					newByteSetList.add(index, b);
					newNode = new Node(newByteSetList);
					if (index == nodeByteSetList.size()) {
						newNode.getKeySet().put(nodeByteSetList, mappa);
						queue.add(newNode);
					} else
						queue.get(queue.indexOf(newNode)).getKeySet().put(nodeByteSetList, mappa);
				}
			}
		}
		return System.currentTimeMillis() - s;
	}

	private void applyInference(List<Byte> lhs, List<Byte> rhs, Map<List<Byte>, Set<Map.Entry<List<Byte>, Byte>>> mappa) {
		if (Collections.disjoint(lhs, rhs)) {
			List<Byte> lista;
			List<Byte> support = new ArrayList<Byte>(lhs);
			List<Byte> support2 = new ArrayList<Byte>(rhs);
			support.addAll(rhs);
			support2.addAll(lhs);
			byte[] array1, array2;
			array1 = new byte[support.size()];
			for (int i = 0; i < support.size(); i++)
				array1[i] = support.get(i);

			array2 = new byte[support2.size()];
			for (int i = 0; i < support2.size(); i++)
				array2[i] = support2.get(i);

			signalFoundOrderDependency(array1, array2, ComparisonOperator.SMALLER_EQUAL, OrderType.LEXICOGRAPHICAL);
			signalFoundOrderDependency(array2, array1, ComparisonOperator.SMALLER_EQUAL, OrderType.LEXICOGRAPHICAL);
			// orderDependency.add(new SimpleEntry<List<Byte>, List<Byte>>(support, support2));
			// orderDependency.add(new SimpleEntry<List<Byte>, List<Byte>>(support2, support));

			int flag = 0;
			boolean foundSupport = false, foundSupport2 = false;
			for (Map.Entry<List<Byte>, Byte> entry : mappa.get(lhs)) {
				lista = entry.getKey();
				if (!foundSupport) {
					if (lista.equals(support)) {
						array1 = new byte[lhs.size()];
						for (int i = 0; i < lhs.size(); i++)
							array1[i] = lhs.get(i);

						array2 = new byte[support.size()];
						for (int i = 0; i < support.size(); i++)
							array2[i] = support.get(i);

						signalFoundOrderDependency(array1, array2, ComparisonOperator.SMALLER_EQUAL, OrderType.LEXICOGRAPHICAL);
						// orderDependency.add(new SimpleEntry<List<Byte>, List<Byte>>(lhs, support));
						applyInference(lhs, support, mappa);
						entry.setValue((byte) 2);
						foundSupport = true;
						flag++;
						if (flag == 2)
							break;
					}
				}
				if (!foundSupport2) {
					if (lista.equals(support2)) {
						array1 = new byte[lhs.size()];
						for (int i = 0; i < lhs.size(); i++)
							array1[i] = lhs.get(i);

						array2 = new byte[support2.size()];
						for (int i = 0; i < support2.size(); i++)
							array2[i] = support2.get(i);

						signalFoundOrderDependency(array1, array2, ComparisonOperator.SMALLER_EQUAL, OrderType.LEXICOGRAPHICAL);
						// orderDependency.add(new SimpleEntry<List<Byte>, List<Byte>>(lhs, support2));
						applyInference(lhs, support2, mappa);
						entry.setValue((byte) 2);
						foundSupport2 = true;
						flag++;
						if (flag == 2)
							break;
					}
				}
			}
			flag = 0;
			foundSupport = false;
			foundSupport2 = false;
			if (mappa.containsKey(support)) {
				for (Map.Entry<List<Byte>, Byte> entry : mappa.get(support)) {
					lista = entry.getKey();
					if (!foundSupport) {
						if (lista.equals(rhs)) {
							array1 = new byte[support.size()];
							for (int i = 0; i < support.size(); i++)
								array1[i] = support.get(i);

							array2 = new byte[rhs.size()];
							for (int i = 0; i < rhs.size(); i++)
								array2[i] = rhs.get(i);

							signalFoundOrderDependency(array1, array2, ComparisonOperator.SMALLER_EQUAL, OrderType.LEXICOGRAPHICAL);
							// orderDependency.add(new SimpleEntry<List<Byte>, List<Byte>>(support, rhs));
							applyInference(support, rhs, mappa);
							entry.setValue((byte) 2);
							foundSupport = true;
							flag++;
							if (flag == 2)
								break;
						}
					}
					if (!foundSupport2) {
						if (lista.equals(support2)) {
							entry.setValue((byte) 2);
							foundSupport2 = true;
							flag++;
							if (flag == 2)
								break;
							break;
						}
					}
				}
			}

			flag = 0;
			foundSupport = false;
			foundSupport2 = false;
			if (mappa.containsKey(support2)) {
				for (Map.Entry<List<Byte>, Byte> entry : mappa.get(support2)) {
					lista = entry.getKey();
					if (!foundSupport) {
						if (lista.equals(lhs)) {
							// System.out.println("PRINT 2: " + support2 + " => " + rhs);
							array1 = new byte[support2.size()];
							for (int i = 0; i < support2.size(); i++)
								array1[i] = support2.get(i);

							array2 = new byte[rhs.size()];
							for (int i = 0; i < rhs.size(); i++)
								array2[i] = rhs.get(i);

							signalFoundOrderDependency(array1, array2, ComparisonOperator.SMALLER_EQUAL, OrderType.LEXICOGRAPHICAL);
							// orderDependency.add(new SimpleEntry<List<Byte>, List<Byte>>(support2, lhs));
							applyInference(support2, lhs, mappa);
							entry.setValue((byte) 2);
							break;
						}
					}
					if (!foundSupport2) {
						if (lista.equals(support2)) {
							entry.setValue((byte) 2);
							foundSupport2 = true;
							flag++;
							if (flag == 2)
								break;
							break;
						}
					}
				}
			}
		}
	}

	private boolean checkConstantness(List<Byte> list) {
		for (Byte b : list)
			if (!dataByColumns.get(b).isConstant())
				return false;
		return true;
	}

	private boolean anyColumnIsUnique(List<Byte> list) {
		for (Byte b : list)
			if (dataByColumns.get(b).isUnique())
				return true;
		return false;
	}

	public void performWalk(byte[] columns) {
		long second = getTimeForWhileApproach(columns);
		System.out.println("===================================");
		System.out.println("===================================");
		System.out.println("Approach " + (second) + "ms");
	}

	public void signalFoundOrderDependency(final byte[] lhs, final byte[] rhs, final OrderDependency.ComparisonOperator comparisonOperator, final OrderDependency.OrderType orderType) {
		// System.out.println("Table {} contains a valid OD: {} ~> {} (Comparison operator: {}, ordering: {})", this.tableName, this.permutationToColumnNames(lhs), this.permutationToColumnNames(rhs), comparisonOperator.name(), orderType.name()));
		final OrderDependency orderDependency = new OrderDependency(new ColumnPermutation(new ColumnIdentifier(this.tableName, this.permutationToColumnNames(lhs))), new ColumnPermutation(new ColumnIdentifier(this.tableName, this.permutationToColumnNames(rhs))), orderType, comparisonOperator);
		try {
			this.resultReceiver.receiveResult(orderDependency);
		} catch (final CouldNotReceiveResultException e) {
			e.printStackTrace();
		}
	}

	public String permutationToColumnNames(final byte[] permutation) {
		if (permutation.length == 0) {
			return "[]";
		}
		final int[] colIndices = new int[permutation.length];
		final StringBuilder sb = new StringBuilder();
		sb.append("[");
		for (int i = 0; i < colIndices.length; i++) {
			sb.append(this.columnNames.get(permutation[i]));
			sb.append(",");
		}
		sb.deleteCharAt(sb.length() - 1);
		sb.append("]");
		return sb.toString();
	}

	// protected void outputSomething() throws CouldNotReceiveResultException {
	// ColumnPermutation lhs = new ColumnPermutation(
	// new ColumnIdentifier(this.relationName,
	// this.columnNamesByte.get(0),toString()),
	// new ColumnIdentifier(this.relationName, this.columnNames.get(1)),
	// new ColumnIdentifier(this.relationName, this.columnNames.get(2)));
	// ColumnPermutation rhs = new ColumnPermutation(
	// new ColumnIdentifier(this.relationName, this.columnNames.get(3)),
	// new ColumnIdentifier(this.relationName, this.columnNames.get(4)),
	// new ColumnIdentifier(this.relationName, this.columnNames.get(5)));
	// OrderDependency od = new OrderDependency(lhs, rhs,
	// OrderDependency.OrderType.LEXICOGRAPHICAL,
	// OrderDependency.ComparisonOperator.STRICTLY_SMALLER);
	//
	// this.resultReceiver.receiveResult(od);
	// }
}
