package de.metanome.algorithms.odin0.order;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import de.metanome.algorithms.odin0.order.sorting.partitions.RowIndexedValue;
import de.metanome.algorithms.odin0.order.sorting.value.RowValue;

public class Column {
	private boolean						isUnique;
	private boolean						isConstant;
	private List<RowIndexedValue>		data;
	// private List<RowIndexedValue> orderedData;
	private Map<RowValue, List<Long>>	invertedIndex;

	// private Map<Long, Integer> srotolamento;

	public Column() {
		this.data = new ArrayList<RowIndexedValue>();
		this.invertedIndex = new HashMap<RowValue, List<Long>>();
	}

	public void endPreliminaries(int totalRows) {
		this.checkConstantness();
		this.checkUniqueness(totalRows);
		// this.createSortedData();
	}

	public void add(RowIndexedValue riv, RowValue rv, long tupleId) {
		this.data.add(riv);
		if (!this.invertedIndex.containsKey(rv))
			this.invertedIndex.put(rv, new ArrayList<Long>());
		this.invertedIndex.get(rv).add(tupleId);
	}

	public void checkConstantness() {
		setConstant(this.invertedIndex.size() == 1);
	}

	public void checkUniqueness(int totalRows) {
		setUnique(totalRows == this.invertedIndex.size());
	}

	// public void createSortedData() {
	// orderedData = new ArrayList<RowIndexedValue>(data);
	// Collections.sort(orderedData, new Comparator<RowIndexedValue>() {
	// public int compare(RowIndexedValue o1, RowIndexedValue o2) {
	// return o1.compareTo(o2);
	// }
	// });
	// }

	public boolean isUnique() {
		return isUnique;
	}

	public void setUnique(boolean isUnique) {
		this.isUnique = isUnique;
	}

	public boolean isConstant() {
		return isConstant;
	}

	public void setConstant(boolean isConstant) {
		this.isConstant = isConstant;
	}

	public List<RowIndexedValue> getData() {
		return data;
	}

	public void setData(List<RowIndexedValue> data) {
		this.data = data;
	}

	public Map<RowValue, List<Long>> getInvertedIndex() {
		return invertedIndex;
	}

	public void setInvertedIndex(Map<RowValue, List<Long>> invertedIndex) {
		this.invertedIndex = invertedIndex;
	}

//	public List<RowIndexedValue> getOrdererData() {
//		return orderedData;
//	}
//
//	public void setOrdererData(List<RowIndexedValue> ordererData) {
//		this.orderedData = ordererData;
//	}
//
//	public List<RowIndexedValue> getOrderedData() {
//		return orderedData;
//	}
//
//	public void setOrderedData(List<RowIndexedValue> orderedData) {
//		this.orderedData = orderedData;
//	}
//
//	public Map<Long, Integer> getSrotolamento() {
//		return srotolamento;
//	}
//
//	public void setSrotolamento(Map<Long, Integer> srotolamento) {
//		this.srotolamento = srotolamento;
//	}
}
