package de.metanome.algorithms.odin0.order.sorting.partitions;

import java.util.Date;

public class RowIndexedDateValue extends RowIndexedValue {
	public final Date	value;

	public RowIndexedDateValue(final long index, final Date value) {
		this.index = index;
		this.value = value;
	}

	@Override
	public int compareTo(RowIndexedValue o) {
		return value.compareTo(((RowIndexedDateValue) o).value);
	}

	@Override
	public String toString() {
		return value.toString();
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((value == null) ? 0 : value.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		RowIndexedDateValue other = (RowIndexedDateValue) obj;
		if (value == null) {
			if (other.value != null)
				return false;
		} else if (!value.equals(other.value))
			return false;
		return true;
	}	
}
