package de.metanome.algorithms.odin0.order.sorting.partitions;

public class RowIndexedDoubleValue extends RowIndexedValue {
	public final Double	value;

	public RowIndexedDoubleValue(final long index, final Double value) {
		this.index = index;
		this.value = value;
	}

	@Override
	public int compareTo(RowIndexedValue o) {
		return value.compareTo(((RowIndexedDoubleValue) o).value);
	}

	@Override
	public String toString() {
		return value.toString();
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((value == null) ? 0 : value.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (obj == null)
			return false;
		if (obj == this)
			return true;
		if (!(obj instanceof RowIndexedDoubleValue))
			return false;
		RowIndexedDoubleValue other = (RowIndexedDoubleValue) obj;
		if (this.value.equals(other.value))
			return true;
		return false;
	}
}
