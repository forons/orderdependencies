package de.metanome.algorithms.odin0.order.sorting.partitions;

public abstract class RowIndexedValue implements Comparable<RowIndexedValue> {
	public long	index	= -1;

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		RowIndexedValue other = (RowIndexedValue) obj;
		if (index != other.index)
			return false;
		return true;
	}
}
