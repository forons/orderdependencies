package de.metanome.algorithms.odin0.order.sorting.value;

public class RowLongValue extends RowValue {
	public final Long	value;

	public RowLongValue(final Long value) {
		this.value = value;
	}

	@Override
	public String toString() {
		return value.toString();
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((value == null) ? 0 : value.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (obj == null)
			return false;
		if (obj == this)
			return true;
		if (!(obj instanceof RowLongValue))
			return false;
		RowLongValue other = (RowLongValue) obj;
		if (this.value.equals(other.value))
			return true;
		return false;
	}

	@Override
	public int compareTo(RowValue o) {
		return value.compareTo(((RowLongValue) o).value);
	}

}
