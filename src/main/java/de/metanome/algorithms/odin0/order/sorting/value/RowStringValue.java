package de.metanome.algorithms.odin0.order.sorting.value;

public class RowStringValue extends RowValue {
	public final String	value;

	public RowStringValue(final String value) {
		this.value = value;
	}

	@Override
	public int compareTo(RowValue o) {
		return value.compareTo(((RowStringValue) o).value);
	}

	@Override
	public String toString() {
		return value.toString();
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((value == null) ? 0 : value.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		RowStringValue other = (RowStringValue) obj;
		if (value == null) {
			if (other.value != null)
				return false;
		} else if (!value.equals(other.value))
			return false;
		return true;
	}
}
